<?php


namespace Cblink\MeituanDispatch;


use Hanson\Foundation\Foundation;

/**
 * Class OrderDispatch
 * @package Cblink\MeituanDispatch
 *
 * @property Test $test
 *
 * @method array createShop($params)
 * @method array updateShop($params)
 * @method array queryShop($params)
 */
class ShopDispatch extends Foundation
{

    private $shop;

    protected $providers = [
        TestServiceProvider::class
    ];

    public function __construct($config)
    {
        parent::__construct($config);
        $this->shop = new Shop($this, $config['app_key'], $config['secret']);
    }

    public function __call($name, $arguments)
    {
        return $this->shop->{$name}(...$arguments);
    }
}