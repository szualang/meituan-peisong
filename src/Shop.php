<?php


namespace Cblink\MeituanDispatch;


class Shop extends Api
{

    /**
     * 门店创建
     * 合作方提交门店信息（注意测试推单仍然用系统默认test_0001门店）
     *
     * @param array $params
     * @return mixed
     * @throws MeituanDispatchException
     */
    public function createShop(array $params)
    {
        return $this->request('shop/create', $params);
    }

    /**
     * 门店修改
     * 合作方提交门店信息（注意测试推单仍然用系统默认test_0001门店）；门店信息不能同时为空，则返回报错；若参数和原门店数据一致，则返回成功；
     * 若存在门店修改未审核任务时，则返回报错。
     *
     * @param array $params
     * @return mixed
     * @throws MeituanDispatchException
     */
    public function updateShop(array $params)
    {
        return $this->request('shop/update', $params);
    }

    /**
     * 查询门店基本信息
     *
     * @param array $params
     * @return mixed
     * @throws MeituanDispatchException
     */
    public function queryShop(array $params)
    {
        return $this->request('shop/query', $params);
    }

}