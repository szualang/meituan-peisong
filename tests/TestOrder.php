<?php

namespace Cblink\MeituanDispatch\Tests;

use Cblink\MeituanDispatch\OrderDispatch;
use Cblink\MeituanDispatch\ShopDispatch;
use PHPUnit\Framework\TestCase;


class TestOrder extends TestCase
{
    protected $secret = '`QqT}H)9%|%gT{_e.RG6U::V96zEy+:ds+82m-lK3/y=6rWG^DVU4lpgZ:aNBG0H';

    protected $appKey = '93f1c5add8004b6eb8cb554896f35638';

    /**
     * 创建订单
     *
     */
    public function testCreateOrShop()
    {
        $goods[] = [
            'goodCount' => 2,
            'goodName' => '测试商品',
        ];

        $data = [
            'delivery_id' => 1,
            'order_id' => 1,
            'poi_seq' => '40001', // 美团的流水号加4为前缀
            'shop_id' => 'test_0001',
            'delivery_service_code' => 4011,
            'receiver_name' => 'test',
            'receiver_address' => '中国',
            'receiver_phone' => '13800138000',
            'receiver_lng' => (int)(113.957613 * pow(10, 6)),
            'receiver_lat' => (int)(22.538135 * pow(10, 6)),
            'goods_value' => 100,
            'goods_weight' => 1,
            'goods_detail' => json_encode(['goods' => $goods]),
            'outer_order_source_desc' => 202,
        ];
        $app = new OrderDispatch(['app_key' => $this->appKey, 'secret' => $this->secret, 'debug' => false]);

        $result = $app->createByShop($data);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 查询订单状态
     *
     */
    public function testQueryStatus()
    {
        $result = (new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false
        ]))->queryStatus([
            'mt_peisong_id' => '1616381324271001396',
            'delivery_id' => '1'
        ]);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 删除订单
     */
    public function testDelete()
    {
        $result = (new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->delete([
            'mt_peisong_id' => '',
            'delivery_id' => '',
            'cancel_reason_id' => '',   // 取消原因类别，默认为接入方原因
            'cancel_reason' => '',  // 详细取消原因，最长不超过256个字符
        ]);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 评价骑手
     */
    public function testEvaluate()
    {
        $result = (new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->evaluate([
            'mt_peisong_id' => '',
            'delivery_id' => '',
            'score' => '', // 评分（5分制）
            'comment_content' => '',  // 评论内容（评论的字符长度需小于1024）
        ]);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 配送能力校验
     */
    public function testCheck()
    {
        $result = (new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->check([
            'shop_id' => '',
            'delivery_service_code' => '',  // 配送服务代码
            'receiver_address' => '', // 收件人地址，最长不超过 512 个字符
            'receiver_lng' => '',  // 收件人经度
            'receiver_lat' => '',   // 收件人纬度
            'coordinate_type' => '',    // 坐标类型
            'check_type' => '', // 预留字段，方便以后扩充校验规则，check_type = 1
            'mock_order_time' => '',    // 模拟发单时间，时区为 GMT+8，当前距离 Epoch（1970年1月1日) 以秒计算的时间
        ]);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 获取骑手当前位置
     */
    public function testLocation()
    {
        $result = (new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->location([
            'delivery_id' => '',
            'mt_peisong_id' => '',  // 美团配送内部订单 id
        ]);

        $this->assertArrayHasKey('data', $result);
    }

    public function testCreateShop()
    {
        $param = array(
            'shop_id' => 'test_0013',
            'shop_name' => '门店的名称',
            'category' => 120,
            'second_category' => 120011,
            'contact_name' => '123456',
            'contact_phone' => '13570812157',
            'shop_address' => '广东省深圳市福田区华强北上步工业区南方大厦',
            'shop_address_detail' => 'B505',
            'shop_lng' => 114085325,
            'shop_lat' => 22548070,
            'coordinate_type' => 0,
            'delivery_service_codes' => '4002',
            'business_hours' => '[{"beginTime":"00:00","endTime":"24:00"}]',
        );


        $result = (new ShopDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->createShop($param);

        $this->assertArrayHasKey('data', $result);
    }

    public function testUpdateShop()
    {
        $param = array(
            'shop_id' => 'test_0001',
            'shop_name' => '门店的名称2',
            'category' => 120,
            'second_category' => 120011,
            'contact_name' => '123456',
            'contact_phone' => '13570812157',
            'shop_address' => '广东省深圳市福田区华强北上步工业区南方大厦',
            'shop_address_detail' => 'B505',
            'shop_lng' => 114085325,
            'shop_lat' => 22548070,
            'coordinate_type' => 0,
            'delivery_service_codes' => '4002',
            'business_hours' => '[{"beginTime":"00:00","endTime":"24:00"}]',
        );


        $result = (new ShopDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->updateShop($param);

        $this->assertArrayHasKey('data', $result);
    }

    public function testQueryShop()
    {
        $param = array(
            'shop_id' => 'test_0001',
        );


        $result = (new ShopDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => false,
        ]))->queryShop($param);

        $this->assertArrayHasKey('data', $result);
    }

    /**
     * 模拟接单
     * @throws \Cblink\MeituanDispatch\MeituanDispatchException
     */
    public function testOrderArrange()
    {
        # 订单测试接口

        $deliveryId = '2021032314174002';
        $peisongId = '1616480611219001143';

        $dispatch = new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => true,
        ]);


        // 模拟接单
        $result = $dispatch->test->arrange($deliveryId, $peisongId);

        $this->assertEquals(0, $result['code']);
    }

    /**
     * 模拟取货
     * @throws \Cblink\MeituanDispatch\MeituanDispatchException
     */
    public function testOrderPickup()
    {
        # 订单测试接口

        $deliveryId = '2021032314174002';
        $peisongId = '1616480611219001143';

        $dispatch = new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => true,
        ]);


        // 模拟接单
        $result = $dispatch->test->arrange($deliveryId, $peisongId);

        $this->assertEquals(0, $result['code']);
    }


    /**
     * 模拟送达
     * @throws \Cblink\MeituanDispatch\MeituanDispatchException
     */
    public function testOrderDeliver()
    {
        # 订单测试接口

        $deliveryId = '2021032314174002';
        $peisongId = '1616480611219001143';

        $dispatch = new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => true,
        ]);


        // 模拟接单
        $result = $dispatch->test->arrange($deliveryId, $peisongId);

        $this->assertEquals(0, $result['code']);
    }

    /**
     * 模拟改派
     * @throws \Cblink\MeituanDispatch\MeituanDispatchException
     */
    public function testOrderRearrange()
    {
        # 订单测试接口

        $deliveryId = '2021032314174002';
        $peisongId = '1616480611219001143';

        $dispatch = new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => true,
        ]);


        // 模拟接单
        $result = $dispatch->test->arrange($deliveryId, $peisongId);

        $this->assertEquals(0, $result['code']);
    }

    /**
     * 模拟上传异常
     * @throws \Cblink\MeituanDispatch\MeituanDispatchException
     */
    public function testOrderReportException()
    {
        # 订单测试接口

        $deliveryId = '2021032314174002';
        $peisongId = '1616480611219001143';

        $dispatch = new OrderDispatch([
            'app_key' => $this->appKey,
            'secret' => $this->secret,
            'debug' => true,
        ]);


        // 模拟接单
        $result = $dispatch->test->arrange($deliveryId, $peisongId);

        $this->assertEquals(0, $result['code']);
    }
}